/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.ws;

import java.util.List;

import com.dungnv.dto.*;

/**
 */
public interface UserWS
{
	public BooleanResponse isExist(String id);
	public UsersResponse findAll(Integer page, Integer pageSize, Long pk, String id, Integer role);
	public IntegerResponse count(Long pk, String id, Integer role);
	public UserResponse save(UserDTO user);
	public UserResponse update(UserDTO user);
	public BooleanResponse delete(List<Long> pks);
}
