/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.ws;

import com.dungnv.dto.*;

/**
 */
public interface AuthenticateWS
{
	public UserResponse login(String id, String password);
}
