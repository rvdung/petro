/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.entity;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import com.dungnv.dto.GroupDTO;

/**
 */
@Entity
@Table(name = "tbl_group", schema="public")
public class GroupEntity extends BaseEntity<GroupDTO>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1696845598735031366L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "pk", nullable = false, unique = true, updatable = false)
	private Long pk;
	@Column(name = "name", unique = true, nullable = true)
	private String name; 
	@Column(name = "description")
	private String description; 

	public GroupEntity() {
	}
	public GroupEntity(Long pk, String name, String description)
	{
		this.pk = pk;
		this.name = name;
		this.description = description;
	}
	public Long getPk()
	{
		return this.pk;
	}
	public void setPk(Long pk)
	{
		this.pk = pk;
	}
	public String getName()
	{
		return this.name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getDescription()
	{
		return this.description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	@Override
	public GroupDTO dtoParser() {
		// TODO Auto-generated method stub
		return null;
	}

}
