/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.entity;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.dungnv.dto.UserDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 */
@Entity
@Table(name = "tbl_user", schema="public")
public class UserEntity extends BaseEntity<UserDTO> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 282796760164730164L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "pk", nullable = false, unique = true, updatable = false)
	private Long pk;
	@Column(name = "id", nullable = false, unique = true)
	private String id;
	@Column(name = "password")
	private String password;
	@Column(name = "role", length = 5)
	private Integer role;

	public UserEntity() {
	}

	public UserEntity(Long pk, String id, String password, Integer role) {
		this.pk = pk;
		this.id = id;
		this.password = password;
		this.role = role;
	}

	public Long getPk() {
		return this.pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRole() {
		return this.role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	@Override
	public UserDTO dtoParser() {
		return new UserDTO(pk, id, password, role);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserEntity other = (UserEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
