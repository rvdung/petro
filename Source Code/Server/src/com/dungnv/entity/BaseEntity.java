package com.dungnv.entity;

import java.io.Serializable;

import com.dungnv.dto.BaseDTO;

public abstract class BaseEntity<DTO extends BaseDTO<?>> implements Serializable {
	public abstract DTO dtoParser();
}
