/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.entity;

import java.io.*;

import javax.persistence.*;

/**
 */
@Embeddable
public class UserGroupKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5917912090880415005L;
	@ManyToOne
	@JoinColumn(name = "group_pk", updatable=false, nullable = false)
	private GroupEntity group;
	@ManyToOne
	@JoinColumn(name = "user_pk", updatable=false, nullable = false)
	private UserEntity user;

	public GroupEntity getGroup() {
		return group;
	}

	public void setGroup(GroupEntity group) {
		this.group = group;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

}
