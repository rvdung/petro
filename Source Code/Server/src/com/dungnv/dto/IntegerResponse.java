/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.dto;

/**
 */
public class IntegerResponse extends BaseResponse<Integer>
{
	public IntegerResponse(Integer result)
	{
		super(result);
	}

	public IntegerResponse(BaseException error)
	{
		super(error);
	}
}
