/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.dto;

/**
 */
public class BaseException extends Throwable
{
	private String errorCode;
	private String errorMessage;
	public String getErrorCode()
	{
		return this.errorCode;
	}
	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}
	public String getErrorMessage()
	{
		return this.errorMessage;
	}
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
	public BaseException(String errorCode, String errorMessage)
	{
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	public BaseException(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public BaseException(Throwable ex)
	{
		this.errorMessage = ex.getMessage();
	}

}
