/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.dto;

/**
 */
public class UserResponse extends BaseResponse<UserDTO> {
	public UserResponse(UserDTO result) {
		super(result);
	}

	public UserResponse(BaseException error) {
		super(error);
	}
}
