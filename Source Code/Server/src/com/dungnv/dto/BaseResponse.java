/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.dto;

/**
 */
public abstract class BaseResponse<T>
{
	private boolean isSuccess;
	private BaseException error;
	private T result;

	public BaseResponse(T result)
	{
		this.isSuccess = true;
		this.result = result;
	}
	public BaseResponse(BaseException error)
	{
		this.isSuccess = false;
		this.error = error;
	}
	public boolean isSuccess()
	{
		return this.isSuccess;
	}
	public void setSuccess(boolean isSuccess)
	{
		this.isSuccess = isSuccess;
	}
	public BaseException getError()
	{
		return this.error;
	}
	public void setError(BaseException error)
	{
		this.error = error;
	}
	public T getResult()
	{
		return this.result;
	}
	public void setResult(T result)
	{
		this.result = result;
	}
}
