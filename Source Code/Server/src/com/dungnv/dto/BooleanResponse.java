/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.dto;

/**
 */
public class BooleanResponse extends BaseResponse<Boolean>
{
	public BooleanResponse(Boolean result)
	{
		super(result);
	}

	public BooleanResponse(BaseException error)
	{
		super(error);
	}
}
