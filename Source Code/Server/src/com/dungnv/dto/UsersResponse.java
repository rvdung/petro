/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.dto;

import java.util.*;

/**
 */
public class UsersResponse extends BaseResponse<List<UserDTO>>
{
	public UsersResponse(List<UserDTO> result)
	{
		super(result);
	}
	public UsersResponse(BaseException error)
	{
		super(error);
	}
}
