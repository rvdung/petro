package com.dungnv.dto;

import java.io.Serializable;

public abstract class BaseDTO<Entity> implements Serializable {
	public abstract Entity entityParser();
}
