/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.dto;

import com.dungnv.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 */
public class UserDTO extends BaseDTO<UserEntity>
{
	private Long pk;
	private String id;
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;
	private Integer role;
	
	public UserDTO() {
	}

	public UserDTO(Long pk, String id, String password, Integer role) {
		this.pk = pk;
		this.id = id;
		this.password = password;
		this.role = role;
	}
	
	public Long getPk()
	{
		return this.pk;
	}
	public void setPk(Long pk)
	{
		this.pk = pk;
	}
	public String getId()
	{
		return this.id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getPassword()
	{
		return this.password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public Integer getRole()
	{
		return this.role;
	}
	public void setRole(Integer role)
	{
		this.role = role;
	}
	@Override
	public UserEntity entityParser() {
		return new UserEntity(pk, id, password, role);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDTO other = (UserDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
