/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.repository;

import org.springframework.data.jpa.repository.*;

import com.dungnv.entity.*;

/**
 */
public interface GroupJpaRepository extends JpaRepository<GroupEntity, Long>
{

}
