/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.repository;

import java.util.List;

import com.dungnv.entity.*;

/**
 */
public interface UserRepository extends BaseRepository<UserEntity> {
	public UserEntity findById(String id) throws Throwable;
	public UserEntity findByPK(Long pk) throws Throwable;
	public UserEntity login(String id, String password) throws Throwable;
	public boolean deleteUser(List<Long> pks) throws Throwable;
}
