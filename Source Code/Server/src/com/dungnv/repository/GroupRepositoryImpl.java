/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.*;

import com.dungnv.entity.GroupEntity;

/**
 */
@Repository
public class GroupRepositoryImpl implements GroupRepository
{
	@Autowired
	private GroupJpaRepository groupJpaRepository;

	@Override
	public List<GroupEntity> findAll(GroupEntity entity, Pageable pageable) throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GroupEntity save(GroupEntity user) throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int count(GroupEntity entity) throws Throwable {
		// TODO Auto-generated method stub
		return 0;
	}
}
