/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import com.dungnv.entity.*;


/**
 */
@Repository
public class UserRepositoryImpl implements UserRepository
{
	private static final Logger logger = LoggerFactory.getLogger(UserRepositoryImpl.class);

	@PersistenceContext(unitName = "mobApiPersistence")
	private EntityManager entityManager;

	@Autowired
	private UserJpaRepository userJpaRepository;


	@Override
	public UserEntity login(String id, String password) throws Throwable {
		List<UserEntity> list = userJpaRepository.login(id, password);
		 return list == null || list.isEmpty() ? null : list.get(0);
	}
	
	@Override
	public List<UserEntity> findAll(UserEntity entity, Pageable pageable) throws Throwable
	{
		
		ExampleMatcher matcher = ExampleMatcher.matching()     
                .withStringMatcher(StringMatcher.CONTAINING)
                .withIgnoreCase(); 
		Example<UserEntity> example = Example.of(entity, matcher);
		return userJpaRepository.findAll(example, pageable).getContent();
	}
	

	@Override
	public int count(UserEntity entity) throws Throwable {
		ExampleMatcher matcher = ExampleMatcher.matching()     
                .withStringMatcher(StringMatcher.CONTAINING)
                .withIgnoreCase(); 
		Example<UserEntity> example = Example.of(entity, matcher);
		return Integer.valueOf(String.valueOf(userJpaRepository.count(example)));
	}
	
	@Override
	public UserEntity findById(String id) throws Throwable
	{
		List<UserEntity> list = userJpaRepository.findById(id);
		return list == null || list.isEmpty() ? null : list.get(0);
	}
	
	@Override
	public UserEntity findByPK(Long pk) throws Throwable {
		return this.userJpaRepository.findById(pk).get();
	}


	@Override
	@Transactional
	public UserEntity save(UserEntity user) throws Throwable {
		try {
			return userJpaRepository.save(user);
		} catch (Exception e) {
			throw e;
		}
		
	}

	@Override
	@Transactional
	public boolean deleteUser(List<Long> pks) throws Throwable {
		try {
			userJpaRepository.deleteUsersWithPKs(pks);
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
}
