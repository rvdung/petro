package com.dungnv.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import com.dungnv.entity.BaseEntity;

public interface BaseRepository<T extends BaseEntity<?>> {
	public List<T> findAll(T entity, Pageable pageable) throws Throwable;
	public int count(T entity) throws Throwable;
	public T save(T user) throws Throwable;
}
