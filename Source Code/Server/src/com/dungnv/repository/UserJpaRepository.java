/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import com.dungnv.entity.*;

/**
 */
public interface UserJpaRepository extends JpaRepository<UserEntity, Long> {
	public List<UserEntity> findById(String id);

	@Query(value = "SELECT u FROM UserEntity u WHERE LOWER(u.id) = LOWER(:id) AND u.password = :password")
	public List<UserEntity> login(@Param("id") final String id, @Param("password") final String password);

	@Modifying
	@Query("DELETE FROM UserEntity u WHERE u.pk IN ?1")
	public void deleteUsersWithPKs(List<Long> ids);
}
