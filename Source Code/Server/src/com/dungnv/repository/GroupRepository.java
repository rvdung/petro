/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.repository;

import com.dungnv.entity.GroupEntity;

/**
 */
public interface GroupRepository extends BaseRepository<GroupEntity>
{

}
