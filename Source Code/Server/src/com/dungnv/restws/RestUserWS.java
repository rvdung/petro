/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.restws;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import com.dungnv.dto.*;
import com.dungnv.service.*;
import com.dungnv.ws.*;

/**
 */
@RestController
@RequestMapping("rest/user")
public class RestUserWS implements UserWS {
	@Autowired
	UserService userService;

	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public BooleanResponse isExist(@PathVariable String id) {
		return userService.isExist(id);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public UsersResponse findAll(@RequestParam Integer page
			, @RequestParam Integer pageSize
			, @RequestParam(required=false) Long pk
			,@RequestParam(required=false) String id
			, @RequestParam(required=false) Integer role) {
		return userService.findAll(page, pageSize, new UserDTO(pk, id, null, role));
	}

	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/count")
	public IntegerResponse count(@RequestParam(required=false) Long pk
			,@RequestParam(required=false) String id
			, @RequestParam(required=false) Integer role) {
		return userService.count(new UserDTO(pk, id, null, role));
	}

	@Override
	@RequestMapping(method = RequestMethod.POST)
	public UserResponse save(@RequestBody UserDTO user) {
		return userService.save(user);
	}
	
	@Override
	@RequestMapping(method = RequestMethod.PUT)
	public UserResponse update(@RequestBody UserDTO user) {
		return userService.save(user);
	}

	@Override
	@RequestMapping(method = RequestMethod.DELETE)
	public BooleanResponse delete(@RequestParam( name="pk", required=false) List<Long> pks) {
		return userService.delete(pks) ;
	}
}
