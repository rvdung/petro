/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.restws;

import org.springframework.beans.factory.annotation.*;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContext;

import com.dungnv.dto.*;
import com.dungnv.service.*;
import com.dungnv.ws.*;

/**
 */
@RestController
@RequestMapping("rest/authen")
public class RestAuthenticateWS implements AuthenticateWS {
	@Autowired
	UserService userService;

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public UserResponse login(@RequestParam String id,@RequestParam  String password) {
		return userService.authen(id, password);
	}
}
