/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.service;

import java.util.List;
import java.util.ArrayList;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import com.dungnv.dto.*;
import com.dungnv.entity.UserEntity;
import com.dungnv.repository.*;

/**
 */
@Service
public class UserServiceImpl implements UserService
{
	@Autowired
	UserRepository userRepository;

	@Override
	public BooleanResponse isExist(String id)
	{
		try
		{
			return new BooleanResponse(Boolean.valueOf(this.userRepository.findById(id) != null));
		}
		catch (Throwable ex)
		{
			if (ex instanceof BaseException)
			{
				return new BooleanResponse((BaseException) ex);
			}
			else
			{
				return new BooleanResponse(new BaseException(ex));
			}
		}
	}

	@Override
	public UsersResponse findAll(Integer page, Integer pageSize, UserDTO searchCondition) {
		try
		{
			List<UserDTO> dtos = new ArrayList<UserDTO>();
			List<UserEntity> entities = this.userRepository.findAll(searchCondition.entityParser(), PageRequest.of(page, pageSize));
			for(UserEntity entity: entities){
				dtos.add(entity.dtoParser());
			}
			return new UsersResponse(dtos);
		}
		catch (Throwable ex)
		{
			if (ex instanceof BaseException)
			{
				return new UsersResponse((BaseException) ex);
			}
			else
			{
				return new UsersResponse(new BaseException(ex));
			}
		}
	}

	@Override
	public IntegerResponse count(UserDTO searchCondition) {
		try
		{
			return new IntegerResponse(this.userRepository.count(searchCondition.entityParser()));
		}
		catch (Throwable ex)
		{
			if (ex instanceof BaseException)
			{
				return new IntegerResponse((BaseException) ex);
			}
			else
			{
				return new IntegerResponse(new BaseException(ex));
			}
		}
	}

	@Override
	public UserResponse save(UserDTO user) {
		try {
			if(user.getPk() == null){
				if(user.getId() == null || user.getId().isEmpty()){
					throw new BaseException(null ,"ID is required");
				}
				
				if(user.getPassword() == null || user.getPassword().isEmpty()){
					throw new BaseException(null ,"password is required");
				}
				
				if(user.getRole() == null){
					throw new BaseException(null ,"role is required");
				}
				
				UserEntity duplicatedUser = userRepository.findById(user.getId());
				if(duplicatedUser!= null){
					throw new BaseException(duplicatedUser.getPk().toString() ,"ID is duplicate");
				}
				

				return new UserResponse(userRepository.save(user.entityParser()).dtoParser());
			} else {
				
				UserEntity availUser = userRepository.findByPK(user.getPk());
				if(availUser== null){
					throw new BaseException(user.getPk().toString() ,"User is not available");
				}
				
				if(user.getId() != null  && !user.getId().isEmpty()){
					UserEntity duplicatedUser = userRepository.findById(user.getId());
					if(duplicatedUser!= null && !duplicatedUser.getPk().equals(user.getPk())){
						throw new BaseException(duplicatedUser.getPk().toString() ,"ID is duplicate");
					}
				}
				
				if(user.getPassword() != null  && !user.getPassword().isEmpty()){
					availUser.setPassword(user.getPassword());
				}
				
				if(user.getId() != null  && !user.getId().isEmpty()){
					availUser.setId(user.getId());
				}
				
				if(user.getRole() != null){
					availUser.setRole(user.getRole());
				}

				return new UserResponse(userRepository.save(availUser).dtoParser());
			}
			
		} catch (Throwable ex) {
			if (ex instanceof BaseException)
			{
				return new UserResponse((BaseException) ex);
			}
			else
			{
				return new UserResponse(new BaseException(ex));
			}
		}
	}

	@Override
	public UserResponse authen(String id, String password) {
		try {
			UserEntity loginUser = userRepository.login(id, password);
			if(loginUser!= null){
				return new UserResponse(loginUser.dtoParser());
			}
			throw new BaseException("id and pass are incorrect");
		} catch (Throwable ex) {
			if (ex instanceof BaseException)
			{
				return new UserResponse((BaseException) ex);
			}
			else
			{
				return new UserResponse(new BaseException(ex));
			}
		}
	}

	@Override
	public BooleanResponse delete(List<Long> pks) {
		try {
			return new BooleanResponse(userRepository.deleteUser(pks));
		} catch (Throwable ex) {
			if (ex instanceof BaseException)
			{
				return new BooleanResponse((BaseException) ex);
			}
			else
			{
				return new BooleanResponse(new BaseException(ex));
			}
		}
	}
}
