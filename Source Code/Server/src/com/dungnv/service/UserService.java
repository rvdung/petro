/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package com.dungnv.service;

import java.util.List;

import com.dungnv.dto.*;

/**
 */
public interface UserService
{
	public UserResponse authen(String id, String password);
	public BooleanResponse isExist(String id);
	public UsersResponse findAll(Integer page, Integer pageSize, UserDTO searchCondition);
	public IntegerResponse count(UserDTO searchCondition);
	public UserResponse save(UserDTO user);
	public BooleanResponse delete(List<Long> pks);
}
