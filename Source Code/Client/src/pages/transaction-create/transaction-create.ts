import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { TransactionType } from '../../enums/transaction-type.enum';
import { Wallet } from '../../models/wallet';
import { User } from '../../models/user';
import { WalletService } from '../../services/wallet.service';
import { Currency } from '../../models/currency';
import { CurrencyService } from '../../services/currency.service';

@IonicPage()
@Component({
  selector: 'page-transaction-create',
  templateUrl: 'transaction-create.html',
})
export class TransactionCreatePage implements OnInit {

  private wallet: Wallet;
  private wallets: Wallet[];
  private currencies: Currency[];
  private transactionType: TransactionType;
  private title: string;
  private selectedWalletId: number;
  private amount: number;
  private price: number;
  private tradeCurrencyId: number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public walletService: WalletService
    , public currencyService: CurrencyService
    , public toastCtrl: ToastController) {
  }

  i

  ngOnInit(): void {
    this.transactionType = this.navParams.get('type');
    this.wallet = this.navParams.get('wallet');
    if (!this.wallet) {
      const user = new User();
      user.setCertificate('hoho', 'hihi');
      this.wallets = this.walletService.getAllWallet(user);
      this.wallet = this.wallets[0];
      this.selectedWalletId = this.wallet.id;
    }

    if (TransactionType.BUY === this.transactionType || TransactionType.SELL === this.transactionType) {
      this.currencies = this.currencyService.getAllCoins();
      this.tradeCurrencyId = this.currencies[0].id;
    }
    if (TransactionType.BUY === this.transactionType) {
      this.title = 'Buy';
      return;
    }
    if (TransactionType.SELL === this.transactionType) {
      this.title = 'Sell';
      return;
    } if (TransactionType.TRANSFER === this.transactionType) {
      this.title = 'Transfer';
      return;
    }
  }
  ionViewDidLoad(): void {
  }
  onChangeWallet(): void {
    this.wallet = this.wallets.find(w => {
      return w.id === this.selectedWalletId;
    });
  }

  onSubmit(): void {
    const toast = this.toastCtrl.create({
      message: this.amount.toFixed.toString(),
      duration: 2000
    });
    toast.present();
  }
}
