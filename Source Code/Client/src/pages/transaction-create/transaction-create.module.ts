import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionCreatePage } from './transaction-create';

@NgModule({
  declarations: [
    TransactionCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionCreatePage),
  ],
})
export class TransactionCreatePageModule {}
