import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { WalletService } from '../../services/wallet.service';
import { Wallet } from '../../models/wallet';
import { User } from '../../models/user';
import { PAGE_DEPOSIT, PAGE_WITHDRAW, PAGE_BUY, PAGE_SELL } from '..';
import { TotpService } from '../../services/totp.service';

/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {

  wallets: Wallet[];
  user: User;
  constructor(public navCtrl: NavController, public navParams: NavParams, private walletService: WalletService
    , public modalCtrl: ModalController) {
      //TODO get user in param, user is provide when finishing loging in
      this.user = this.navParams.get('user');
  }

  ionViewDidLoad() {
    // this.walletService.getWalletFromUserId(this.user.id, function(error, wallets){
    //   if(error){
    //     return;
    //   }
    //   this.wallets = wallets;
    // });
    // console.log('this.wallets', this.wallets);
  }

  openDeposit(wallet: Wallet): void {
    const addModal = this.modalCtrl.create(PAGE_DEPOSIT, {
      wallet: wallet
    });
    addModal.onDidDismiss(wallet => {
      // TODO: update wallet
    });
    addModal.present();
  }

  openWithdraw(wallet: Wallet): void{
    const addModal = this.modalCtrl.create(PAGE_WITHDRAW, {
      wallet: wallet
    });
    addModal.onDidDismiss(wallet => {
      // TODO: update wallet
    });
    addModal.present();
  }

  openBuy(wallet: Wallet): void{
    const addModal = this.modalCtrl.create(PAGE_BUY, {
      wallet: wallet
    });
    addModal.onDidDismiss(wallet => {
      // TODO: update wallet
    });
    addModal.present();
  }
  openSell(wallet: Wallet): void{
    const addModal = this.modalCtrl.create(PAGE_SELL, {
      wallet: wallet
    });
    addModal.onDidDismiss(wallet => {
      // TODO: update wallet
    });
    addModal.present();
  }
}
