import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';

import { Tab1Root, Tab2Root, Tab3Root, PAGE_CREATE_TRANSACTION } from '../';
import { TransactionType } from '../../enums/transaction-type.enum';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";

  constructor(public navCtrl: NavController, public translateService: TranslateService, public modalCtrl: ModalController) {
    translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE']).subscribe(values => {
      this.tab1Title = values['TAB1_TITLE'];
      this.tab2Title = values['TAB2_TITLE'];
      this.tab3Title = values['TAB3_TITLE'];
    });
  }
  buyCoin(): void {
    const addModal = this.modalCtrl.create(PAGE_CREATE_TRANSACTION, {
      type: TransactionType.BUY
    });
    addModal.onDidDismiss(wallet => {
      // TODO: update wallet
    });
    addModal.present();
  }
  sellCoin(): void {
    const addModal = this.modalCtrl.create(PAGE_CREATE_TRANSACTION, {
      type: TransactionType.SELL
    });
    addModal.onDidDismiss(wallet => {
      // TODO: update wallet
    });
    addModal.present();
  }
  transferCoin(): void {
    const addModal = this.modalCtrl.create(PAGE_CREATE_TRANSACTION, {
      type: TransactionType.TRANSFER
    });
    addModal.onDidDismiss(() => {
      // TODO: update wallet
    });
    addModal.present();
  }
}
