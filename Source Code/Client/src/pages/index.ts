
export const PAGE_TUTORIAL = 'TutorialPage';
export const PAGE_WELCOME = 'WelcomePage';
export const PAGE_LOGIN = 'LoginPage';
export const PAGE_SIGNUP = 'SignupPage';
export const PAGE_MENU = 'MenuPage';
export const PAGE_TAB = 'TabsPage';
export const PAGE_DASHBOARD = 'dashboard';
export const PAGE_MARKET = 'MarketPage';
export const PAGE_WALLET = 'WalletPage';
export const PAGE_DEPOSIT = 'DepositPage';
export const PAGE_CREATE_TRANSACTION = 'TransactionCreatePage';
export const PAGE_WITHDRAW = "PageWithdraw";
export const PAGE_BUY = "PageBuy";
export const PAGE_SELL = "PageSell";

export const Tab1Root = PAGE_DASHBOARD;
export const Tab2Root = PAGE_WALLET;
export const Tab3Root = PAGE_MARKET;

