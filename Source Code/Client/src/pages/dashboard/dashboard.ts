import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CurrencyService } from '../../services/currency.service';
import { Coin } from '../../models/coin';
import { PetroRating } from '../../models/rating';

@IonicPage({
  name: 'dashboard'
})
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  segment: number;
  coins: Coin[];
  petroRatings: PetroRating[];
  currencies: String[];
  selectedComparedCurrency: string;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , public currencyService: CurrencyService) {
      this.segment=1;
  }

  ionViewDidLoad() {
    this.currencies = ['USD','EUR','VND','JPY'];
    this.selectedComparedCurrency = 'USD';
    this.coins = this.currencyService.getAllInternationalCoins(this.selectedComparedCurrency);
    this.petroRatings = this.currencyService.getAllPetroRating();
  }

  setSegment($event) {
    this.segment = $event._value;
  }

  isTab(n : number) {
    return this.segment == n;
  }

  update($event) {
    this.coins = this.currencyService.getAllInternationalCoins($event);
  }
}
