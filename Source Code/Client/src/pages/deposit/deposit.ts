import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Clipboard } from '@ionic-native/clipboard';
import { Wallet } from '../../models/wallet';
import { Constants } from '../../configurations/constant';
import * as cb from 'coinbase'
import { DepositeHistoryService } from '../../services/deposite-history.service';
import { WalletService } from '../../services/wallet.service';
import { DepositeHistory } from '../../models/deposite-history';
import { CoinBaseService } from '../../services/coinbase.service';
/**
 * Generated class for the DepositPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-deposit',
    templateUrl: 'deposit.html',
})
export class DepositPage {
    elementType: 'url' | 'canvas' | 'img' = 'url';
    addressId: string;
    wallet: Wallet;
    amount: number = 0;
    gaming: string = 'nes';
    client: cb.Client;
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
        private clipboard: Clipboard, private depositeHistoryService: DepositeHistoryService,
        private walletService: WalletService, private coinbaseService: CoinBaseService) {

        this.wallet = this.navParams.get('wallet');
        this.addressId = this.wallet.address;

    }

    ionViewDidLoad() {

    }

    cancel() {
        this.viewCtrl.dismiss();
    }

    copyToClipboard() {
        this.clipboard.copy(this.addressId);
    }

    onGamingChange() {
        console.log('gaming', this.gaming);
    }

    confirm() {
        var self = this;
        this.depositeHistoryService.getTransactionsByWalletId({ 'walletId': this.wallet.id }, function (error, depositeHistories: DepositeHistory[]) {
            if (error) {
                return;
            }
            self.coinbaseService.getTransactions({}, function (error, cbTransactions: any[]) {
                var unconfirmCBTransactions = [];
                for (var cbTransaction of cbTransactions) {
                    if (self.containsCBTransaction(depositeHistories , cbTransaction.id)) {
                        continue;
                    }
                    unconfirmCBTransactions.push(cbTransaction);
                }
                for(var unconfirm of unconfirmCBTransactions){
                    if(unconfirm.status === 'completed'){
                        //TODO perform update wallet in DB
                        //TODO perform update wallet in history
                        //unconfirm.amount.amount , unconfirm.amount.currency
                    }
                }
            });
        });

    }

    containsCBTransaction(depositeHistories: DepositeHistory[], cbTransactionId): boolean {
        for (var depositeHistory of depositeHistories) {
            if (depositeHistory.cb_transactionId == cbTransactionId) {
                return true;
            }
        }
        return false;
    }


}
