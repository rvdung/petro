import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DepositPage } from './deposit';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DepositPage,
  ],
  imports: [
    IonicPageModule.forChild(DepositPage),
    TranslateModule.forChild(),
    NgxQRCodeModule
  ]
})
export class DepositPageModule { }
