import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { GoogleAuthenModule } from '../../components/google-authen/google-authen.module';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    TranslateModule.forChild(),
    GoogleAuthenModule
  ],
  exports: [
    LoginPage
  ]
})
export class LoginPageModule { }
