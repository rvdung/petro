import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { CurrencyService } from './currency.service';
import { Transaction } from '../models/transaction';


@Injectable()
export class TransactionService {

  constructor() { }

  getAvailableTransactions(user: User, page: number, pageSize: number): Transaction[] {
    //TODO: mockup data
    const trans: Transaction[] = [];
   // const coinsOfPetro = this.currencyService.getAllCoins().filter(c => c.type === CurrencyType.DIGITAL_OF_EXCHANGE);
    for (let i = 0; i < pageSize; i++) {
      const tran = new Transaction();
      tran.id = page + i;
      trans.push(tran);
    }
    return trans;
  }
}
