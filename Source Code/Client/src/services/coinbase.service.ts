import { Injectable } from '@angular/core';
import { Wallet } from '../models/wallet';
import { Constants } from '../configurations/constant';
import * as cb from 'coinbase'

@Injectable()
export class CoinBaseService {

    private static readonly client = new cb.Client({ 'apiKey': Constants.API_KEY, 'apiSecret': Constants.API_SECRET, 'strictSSL': false });

    constructor() {

    }
    //param{'accountId': ..., 'addressId': ...}
    getTransactions(params, callback) {
        CoinBaseService.client.getAccount(params.accountId, function (error, account) {
            if (error) {
                console.log(error);
                callback(error, null);
                return;
            }
            account.getAddress(params.addressId, function (err, address) {
                if (error) {
                    console.log(error);
                    callback(error,null);
                    return;
                }
                address.getTransactions({}, function (err, transactions){
                    if(error){
                        console.log(err);
                        callback(error,null);
                        return;
                    }
                    callback(null, transactions);
                });
            });
        });
    }

}