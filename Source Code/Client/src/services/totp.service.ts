import { Injectable } from '@angular/core';
import * as speakeasy from 'speakeasy';


@Injectable()
export class TotpService {

    constructor(){

    }

    generateSecretKey(): any{
        return speakeasy.generateSecret();
    }

    authenticateOTP(secretKey, userOTP):boolean{
        return speakeasy.totp.verify({
            secret: secretKey,
            encoding: 'base32',
            token: userOTP
        });
    }
}