import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DepositeHistory } from '../models/deposite-history';
import { Constants } from '../configurations/constant';

@Injectable()
export class DepositeHistoryService {
    private readonly DEPOSITE_HISTORY_PATH = "";

    constructor(private http: HttpClient){

    }

    getTransactionsByWalletId(params, callback){
        //TODO: complete request url
        const url = Constants.SERVER_URL + this.DEPOSITE_HISTORY_PATH + params.walletId;
        let depositeHistories: DepositeHistory[];
        this.http.get<DepositeHistory[]>(url).subscribe(depositeHistories =>{
            callback(null, depositeHistories);
        },error => {
            callback(error, null)
        });
    }
}