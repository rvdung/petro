import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Currency } from '../models/currency';
import { Coin } from '../models/coin';
import { CurrencyType } from '../enums/currency-type.enum';
import { PetroRating } from '../models/rating';
import { Price } from '../models/price';


@Injectable()
export class CurrencyService {

  coinNames : string[];
  coins : Coin[];
  constructor(private http : HttpClient) {
    this.coinNames = ['BTC', 'ETH'];
   }

  
  getAllInternationalCoins(unit : string): Coin[] {
    this.coins = [];
    this.coinNames.forEach(name => {
      let url = 'https://api.coinbase.com/v2/prices/' + name + '-' + unit + '/spot';
      this.http.get<Price>(url).subscribe(response => {
        // do whatever you want with the response
        console.log(response);
        this.coins.push(new Coin(name,response.data.amount));
     }, error => {
        // handle error here
        // error.status to get the error code
        console.log(error);
     });
      
    });

    return this.coins;
  }


  getAllCurrencies(): Currency[] {
    return [new Currency(1, 'BTC', 'Bitcoin', CurrencyType.DIGITAL)
      , new Currency(2, 'ETH', 'Ethereum', CurrencyType.DIGITAL)
      , new Currency(4, 'P', 'Petro', CurrencyType.DIGITAL_OF_EXCHANGE)
      , new Currency(3, 'USD', 'USD', CurrencyType.PAPER)];
  }

  getAllCoins(): Currency[] {
    return this.getAllCurrencies().filter(c => c.type === CurrencyType.DIGITAL || c.type === CurrencyType.DIGITAL_OF_EXCHANGE);
  }

  getAllPetroRating() : PetroRating[] {    
    return [new PetroRating('P1', 'Bitcoin', '1.03'),
     new PetroRating('P1', 'ETH', '10.03'),
     new PetroRating('P2', 'Bitcoin', '1.03'),
     new PetroRating('P2', 'ETH', '10.03')];
  }

}
