import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Currency } from '../models/currency';
import { CurrencyType } from '../enums/currency-type.enum';
import { User } from '../models/user';
import { CurrencyService } from './currency.service';
import { Wallet } from '../models/wallet';
import { Constants } from '../configurations/constant';


@Injectable()
export class WalletService {

  

  //TODO wallet path
  wallet_path = "/wallet/";
  constructor(private http : HttpClient, private currencyService: CurrencyService) {
        
   }

  getAllWallet(user: User): Wallet[] {
    //TODO: mockup data
    return this.currencyService.getAllCoins().map( (currency, index) => {
      const wallet = new Wallet();
      wallet.id = index;
      wallet.userId = user.id;
      wallet.amount = 500.2354345345;
      wallet.fkCurrency = currency.id;
      wallet.address = '123afsd';
      return wallet;
    });
  }

  getWalletFromUserId(params, callback) {
    //TODO request url
    const url = Constants.SERVER_URL + this.wallet_path + "userId?userId=" + params.id;
    let wallets: Wallet[] ;
    this.http.get<Wallet[]>(url).subscribe(wallets => {
      callback(null, wallets);
    }, error =>{
      callback(error, null);
    });
  }
}
