export enum TransactionType {
    BUY = 'BUY',
    SELL = 'SELL',
    BUY_FROM_PETRO = 'BUY_FROM_PETRO',
    SELL_TO_PETRO = 'SELL_TO_PETRO',
    DEPOSIT = 'DEPOSIT',
    WITHDRAW = 'WITHDRAW',
    TRANSFER = 'TRANSFER'
}