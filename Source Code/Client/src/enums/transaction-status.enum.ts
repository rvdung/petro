export enum TransactionStatus {
    PENDING = 'PENDING',
    ORDERED = 'ORDERED',
    DONE = 'DONE',
    REJECT = 'REJECT'
}