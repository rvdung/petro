import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { GoogleAuthenComponent } from './google-authen';

@NgModule({
  declarations: [
    GoogleAuthenComponent,
  ],
  imports: [
    IonicPageModule.forChild(GoogleAuthenComponent),
    TranslateModule.forChild()
  ],
  exports: [
    GoogleAuthenComponent
  ]
})
export class GoogleAuthenModule { }
