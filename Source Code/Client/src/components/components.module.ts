import { NgModule } from '@angular/core';
import { GoogleAuthenComponent } from './google-authen/google-authen';
@NgModule({
	declarations: [GoogleAuthenComponent],
	imports: [],
	exports: [GoogleAuthenComponent]
})
export class ComponentsModule {}
