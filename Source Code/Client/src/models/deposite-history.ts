export class DepositeHistory {
    constructor() {
    }
}
export interface DepositeHistory {
    id: number;
    fkWallet: number;
    cb_transactionId: string;
    amount: number;
    depositeDate: number;
}