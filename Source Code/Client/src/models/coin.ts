export class Coin {
  constructor(code: string, value: string) {
    this.code = code;
    this.value = value;
  }
}

export interface Coin {
  code: string;
  value: string;
}
