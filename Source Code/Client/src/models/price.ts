export class Price {
  constructor(data: Data) {
    this.data = data;
  }
}

export class Data {
  constructor(amount: string, currency:string) {
    this.amount = amount;
    this.currency = currency;
  }
}

export interface Price {
  data: Data;
}

export interface Data {
  amount:string;
  currency:string;
}
