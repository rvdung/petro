/**
 * A generic model that our Master-Detail pages list, create, and delete.
 *
 * Change "Item" to the noun your app will use. For example, a "Contact," or a
 * "Customer," or a "Animal," or something like that.
 *
 * The Items service manages creating instances of Item, so go ahead and rename
 * that something that fits your app as well.
 */
export class User {

  constructor() {
  }

  public setCertificate(email: string, password: string) {
    this.email = email;
    this.password = password;
  }
}

export interface User {
  id: number;
  password: string;
  fullName: string;
  email: string;
  mobile: string;
  timeCreated: Date;
  nation: string;
  isActive: boolean;
  isDelete: boolean;
  bankCode: string;
  bankName: string;
  bankOwner: string;
}
