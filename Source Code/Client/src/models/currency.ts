import { CurrencyType } from "../enums/currency-type.enum";

export class Currency {
  constructor(id: number, code: string, name: string, type?: CurrencyType) {
    this.id = id;
    this.code = code;
    this.name = name;
    this.type = type;
    this.isExchange = true;
    this.isTransfer = true;
    this.isTrade = true;
    this.isExchange = true;
  }
}

export interface Currency {
  id: number;
  code: string;
  name: string;
  type: CurrencyType;
  isExchange: boolean;
  isTransfer: boolean;
  isTrade: boolean;
  enable: boolean;
}
