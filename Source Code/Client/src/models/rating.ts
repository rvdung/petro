export class PetroRating {
  constructor(currencyFrom: string, currencyTo: string, value: string) {
    this.currencyFrom = currencyFrom;
    this.currencyTo = currencyTo;
    this.value = value;
  }
}

export interface PetroRating {
  currencyFrom: string;
  currencyTo: string;
  value: string;
}
