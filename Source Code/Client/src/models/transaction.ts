import { TransactionStatus } from "../enums/transaction-status.enum";
import { User } from "./user";


export class Transaction {
  constructor() {
  }
}

export interface Transaction {
  id: number;
  userCreatedId: number;
  buyUserId: number;
  buyCurrencyId: number;
  buyCurrencyType: string;
  buyWalletId: number;
  buyAmount: number;
  sellUserId: number;
  sellAmount: number;
  sellCurrencyId: number;
  sellCurrencyType: string;
  sellWalletId: number;
  timeCreated: Date;
  status: TransactionStatus;
  buyRating: number;
  sellRating: number;
  userCreate: User;
}
