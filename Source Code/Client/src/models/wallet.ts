import { Currency } from "./currency";
import { User } from "./user";

export class Wallet {
  constructor() {

  }
}

export interface Wallet {
  id: number;
  userId: number;
  address: string;
  fkCurrency: number;
  amount: number;
}
